#!/bin/bash

# Set the arguments for the Python script:
VGG="./models/vgg_normalised.pth"
CONTENT_DIR="./input/content/trainA/"
STYLE_DIR="./input/style/trainB/"
MAX_ITER=80000
SAVE_MODEL_INTERVAL=100
PTH_DIR="./80000/pth"
LOG_DIR="./80000/log"
TEST_DIR="./80000/test"
BATCH_SIZE=1

# Execute the Python script with the provided arguments
python train.py \
    --vgg "$VGG" \
    --content_dir "$CONTENT_DIR" \
    --style_dir "$STYLE_DIR" \
    --max_iter "$MAX_ITER" \
    --save_model_interval "$SAVE_MODEL_INTERVAL" \
    --pth_dir "$PTH_DIR" \
    --log_dir "$LOG_DIR" \
    --test_dir "$TEST_DIR" \
    --batch_size "$BATCH_SIZE" \
