#!/bin/bash

# Set the arguments for the Python script:
CONTENT="input/content/testA/18995_img00995.png"
STYLE_DIR="input/style/testB/"
DECODER_PATH="./1W5K/decoder_iter_10000.pth"
TRANS_PATH="./1W5K/transformer_iter_10000.pth"
EMBEDDING_PATH="./1W5K/embedding_iter_10000.pth"
OUTPUT="output"

# Execute the Python script with the provided arguments
python test.py \
    --content "$CONTENT" \
    --style_dir "$STYLE_DIR" \
    --decoder_path "$DECODER_PATH" \
    --trans_path "$TRANS_PATH" \
    --embedding_path "$EMBEDDING_PATH" \
    --output "$OUTPUT" \