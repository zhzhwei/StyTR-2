#!/bin/bash

# Set the arguments for the Python script:
CONTENT_DIR="input/content/testA/"
STYLE="input/style/testB/1002_frame0403.png"
DECODER_PATH="./1W5K/decoder_iter_10000.pth"
TRANS_PATH="./1W5K/transformer_iter_10000.pth"
EMBEDDING_PATH="./1W5K/embedding_iter_10000.pth"
OUTPUT="output"

# Execute the Python script with the provided arguments
python test.py \
    --content_dir "$CONTENT_DIR" \
    --style "$STYLE" \
    --decoder_path "$DECODER_PATH" \
    --trans_path "$TRANS_PATH" \
    --embedding_path "$EMBEDDING_PATH" \
    --output "$OUTPUT" \